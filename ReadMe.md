# Go Mongo Base

> Boilerplate for Go and Mongo, with unit testing example.

## Dependencies

- [Go Chi](https://github.com/go-chi/chi)
- [Go Chi CORS](https://github.com/go-chi/cors)
- [Go Chi JWT](https://github.com/go-chi/jwtauth)
- [Unrolled Secure](https://github.com/unrolled/secure)
- [Unrolled Render](https://github.com/unrolled/render)
- [Gorm](https://gorm.io/)
- [Go Resty](https://github.com/go-resty/resty)
- [HTTP Swagger](https://github.com/swaggo/http-swagger)
- [Mongo](https://github.com/mongodb/mongo-go-driver)
- [Testify](https://github.com/stretchr/testify)

## Requirements

- Go 1.16
- Docker

## Tested on

- Ubuntu 20.04

## How To

1. Run `go mod tidy`
2. Copy `.env.example` to `.env`
3. Setup configuration `.env`
4. Run `make swag.all`
5. Run `make test.api.service.verbose`
6. Run `make start`

## Tools

- `make docker.rmi.none`
  : will clear docker images that having `none` tags or name
- `./logs.sh <service-name>`
  : see log for specific service, `service-name` is required
- `./swag-init.sh <service-name>`
  : initiate docs for swagger file, fill the `service-name` if want to specific. By default will generate all if not define. this will require `swag` binary, install it with `go get github.com/swaggo/swag/cmd/swag` or `make swag.install`. For more info please visit [HTTP Swagger](https://github.com/swaggo/http-swagger)
- `make start`
  : rebuild and start all services with `docker-compose`
- `make stop`
  : stop all services
