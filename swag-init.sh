if [ "$1" == "api-service" ]
then
  printf "\n===== Init Swagger Api Service =====\n\n" && \
  swag init -d ./services/api-service -o ./services/api-service/docs --parseDependency
elif [ "$1" == "company-service" ]
then
  printf "\n===== Init Swagger Company Service =====\n\n" && \
  swag init -d ./services/company-service -o ./services/company-service/docs --parseDependency
elif [ "$1" == "provider-service" ]
then
  printf "\n===== Init Swagger Provider Service =====\n\n" && \
  swag init -d ./services/provider-service -o ./services/provider-service/docs --parseDependency
elif [ "$1" == "storage-service" ]
then
  printf "\n===== Init Swagger Storage Service =====\n\n" && \
  swag init -d ./services/storage-service -o ./services/storage-service/docs --parseDependency
elif [ "$1" == "user-service" ]
then
  printf "\n===== Init Swagger User Service =====\n\n" && \
  swag init -d ./services/user-service -o ./services/user-service/docs --parseDependency
elif [ "$1" == "log-service" ]
then
  printf "\n===== Init Swagger Log Service =====\n\n" && \
  swag init -d ./services/log-service -o ./services/log-service/docs --parseDependency
else
  printf "\n===== Init Swagger Api Service =====\n\n" && \
  swag init -d ./services/api-service -o ./services/api-service/docs --parseDependency
  printf "\n===== Init Swagger Company Service =====\n\n" && \
  swag init -d ./services/company-service -o ./services/company-service/docs --parseDependency && \
  printf "\n===== Init Swagger Provider Service =====\n\n" && \
  swag init -d ./services/provider-service -o ./services/provider-service/docs --parseDependency && \
  printf "\n===== Init Swagger Storage Service =====\n\n" && \
  swag init -d ./services/storage-service -o ./services/storage-service/docs --parseDependency && \
  printf "\n===== Init Swagger User Service =====\n\n" && \
  swag init -d ./services/user-service -o ./services/user-service/docs --parseDependency
  printf "\n===== Init Swagger Log Service =====\n\n" && \
  swag init -d ./services/log-service -o ./services/log-service/docs --parseDependency
fi
