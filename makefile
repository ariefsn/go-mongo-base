swag.install:
	go get -u github.com/swaggo/swag/cmd/swag

swag.api.service:
	printf "\n===== Init Swagger Api Service =====\n\n" && \
  swag init -d ./services/api-service -o ./services/api-service/docs --parseDependency

swag.all:
	printf "\n===== Init Swagger Api Service =====\n\n" && \
  swag init -d ./services/api-service -o ./services/api-service/docs --parseDependency

start:
	docker-compose up --build -d

start.with.swag:
	make swag.all
	make swag.start

stop:
	docker-compose down

docker.rmi.none:
	docker rmi `docker images | grep "<none>" | awk {'print $3'}`

remove.volumes:
	rm -rf volumes/

test.api.service:
	go test -coverprofile ./services/api-service/coverage  ./services/api-service/controllers/... ./services/api-service/repositories/...

test.api.service.verbose:
	go test -coverprofile ./services/api-service/coverage  ./services/api-service/controllers/... ./services/api-service/repositories/... -v