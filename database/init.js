db.createUser(
  {
      user: "ayik",
      pwd: "Password.123",
      roles: [
          {
              role: "readWrite",
              db: "dbGoTest"
          }
      ]
  }
);

db.users.createIndex({ "email": 1 }, { unique: true });
db.users.insert({
  "email": "root@mail.com",
  "firstName": "Super",
  "lastName": "User"
});
