package helper

import (
	"net/http"
	"strings"
)

type Message struct {
	Duplicate            string
	DuplicateId          string
	MethodNotAllowed     string
	RouteNotFound        string
	UserEmailRegistered  string
	DataNotFound         string
	EmailRequired        string
	PasswordRequired     string
	InvalidCredentials   string
	UserUnauthorized     string
	EmptyResponse        string
	GroupStorageRequired string
	FileTooBig           string
	FileTypeInvalid      string
	FileNotExists        string
	FileNameRequired     string
	UnmarshalFailed      string
}

func NewMessage() *Message {
	m := new(Message)

	m.Duplicate = "duplicate data"
	m.DuplicateId = "duplicate id"
	m.MethodNotAllowed = "method not allowed"
	m.RouteNotFound = "route not found"
	m.UserEmailRegistered = "email registered"
	m.DataNotFound = "data not found"
	m.EmailRequired = "email is required"
	m.PasswordRequired = "password is required"
	m.InvalidCredentials = "invalid credentials"
	m.UserUnauthorized = "user is unauthorized"
	m.EmptyResponse = "service response is nil"
	m.GroupStorageRequired = "group storage is required"
	m.FileTooBig = "file size too big"
	m.FileTypeInvalid = "file type is not valid"
	m.FileNotExists = "file not exists"
	m.FileNameRequired = "file name is required"
	m.UnmarshalFailed = "cannot unmarshal"

	return m
}

func MessageCode(message string) int {
	msg := NewMessage()

	type messageCode struct {
		code     int
		messages []string
	}

	messageCodes := []messageCode{
		{
			code: http.StatusMethodNotAllowed,
			messages: []string{
				msg.MethodNotAllowed,
			},
		},
		{
			code: http.StatusNotFound,
			messages: []string{
				msg.DataNotFound,
				msg.RouteNotFound,
				msg.FileNotExists,
			},
		},
		{
			code: http.StatusUnprocessableEntity,
			messages: []string{
				msg.EmailRequired,
				msg.FileNameRequired,
				msg.GroupStorageRequired,
				msg.PasswordRequired,
				msg.FileTooBig,
				msg.FileTypeInvalid,
			},
		},
		{
			code: http.StatusUnauthorized,
			messages: []string{
				msg.InvalidCredentials,
			},
		},
	}

	var code int

	for _, mc := range messageCodes {
		for _, m := range mc.messages {
			if strings.Contains(strings.ToLower(m), strings.ToLower(message)) {
				code = mc.code
				break
			}
		}
		if code > 0 {
			break
		}
	}

	if code > 0 {
		return code
	}

	return http.StatusInternalServerError
}
