package helper

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	"github.com/google/uuid"
)

func UploadFiles(r *http.Request, group string) ([]string, error) {
	uploadPath := GetEnv().Upload.Path
	// maxSize := GetEnv().Upload.MaxSize

	dirPath := filepath.Join(uploadPath, group)

	fileNames := []string{}

	reader, err := r.MultipartReader()

	if err != nil {
		return fileNames, err
	}

	for {
		part, err := reader.NextPart()

		if err == io.EOF {
			break
		}

		newName := uuid.NewString()
		ext := strings.ToLower(filepath.Ext(part.FileName()))
		newFullName := fmt.Sprintf("%s.%s", newName, ext)

		fileLocation := filepath.Join(dirPath, newFullName)

		dst, err := os.Create(fileLocation)

		if dst != nil {
			defer dst.Close()
		}

		if err != nil {
			return []string{}, err
		}

		if _, err := io.Copy(dst, part); err != nil {
			return []string{}, err
		}

		fileNames = append(fileNames, newFullName)
	}

	return fileNames, nil
}

func UploadFile(r *http.Request, group, fileName string) (string, error) {
	uploadPath := GetEnv().Upload.Path
	maxSize := GetEnv().Upload.MaxSize

	if err := r.ParseMultipartForm(int64(maxSize)); err != nil {
		return "", err
	}

	// parse and validate file and post parameters
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		return "", err
	}

	defer file.Close()

	// Get and print out file size
	fileSize := fileHeader.Size

	fmt.Printf("File size (bytes): %v\n", fileSize)

	// validate file size
	if fileSize > int64(maxSize) {
		return "", errors.New(NewMessage().FileTooBig)
	}

	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		return "", err
	}

	// check file type, detectcontenttype only needs the first 512 bytes
	fileType := http.DetectContentType(fileBytes)

	fileTypeCheck := false

	for _, v := range GetEnv().Upload.Types {
		if v == fileType {
			fileTypeCheck = true
			break
		}
	}

	if !fileTypeCheck {
		return "", errors.New(NewMessage().FileTypeInvalid)
	}

	if fileName == "" || strings.ToLower(fileName) == "undefined" {
		fileName = uuid.NewString()
	}

	fileEndings, err := mime.ExtensionsByType(fileType)

	if err != nil {
		return "", err
	}

	dirPath := filepath.Join(uploadPath, group)

	// check dir
	_, err = os.Stat(dirPath)

	if os.IsNotExist(err) {
		err = os.MkdirAll(dirPath, os.ModeDir)

		if err != nil {
			return "", err
		}
	}

	newPath := filepath.Join(dirPath, fileName+fileEndings[0])

	fmt.Printf("FileType: %s, File: %s\n", fileType, newPath)

	// write file
	newFile, err := os.Create(newPath)

	if err != nil {
		return "", err
	}

	defer newFile.Close() // idempotent, okay to call twice

	if _, err := newFile.Write(fileBytes); err != nil || newFile.Close() != nil {
		return "", err
	}

	return fileName + fileEndings[0], nil
}

func DownloadFileTemp(res *http.Response, fileName string) (*os.File, string, error) {
	path := filepath.Join("temp", fileName)

	// check dir
	_, err := os.Stat(path)

	var file *os.File

	if os.IsNotExist(err) {
		file, err = os.Create(path)
	} else {
		file, err = os.Open(path)
	}

	if file != nil {
		defer file.Close()
	}

	if err != nil {
		return nil, "", err
	}

	_, err = io.Copy(file, res.Body)

	if err != nil {
		return nil, "", err
	}

	return file, path, nil
}

func DownloadFile(r *http.Request, group, fileName string) (*os.File, string, error) {
	path := filepath.Join(GetEnv().Upload.Path, group, fileName)

	// check dir
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		return nil, "", errors.New(NewMessage().FileNotExists)
	}

	file, err := os.Open(path)

	if file != nil {
		defer file.Close()
	}

	if err != nil {
		return nil, "", err
	}

	return file, path, nil
}

func RemoveFile(r *http.Request, group, fileName string) (string, error) {
	path := filepath.Join(GetEnv().Upload.Path, group, fileName)

	// check dir
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		return "", errors.New(NewMessage().FileNotExists)
	}

	err = os.Remove(path)

	if err != nil {
		return "", err
	}

	return fileName, nil
}
