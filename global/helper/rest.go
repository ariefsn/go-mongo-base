package helper

import (
	"encoding/json"
	"go-mongo/global/models"
	"net/http"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
)

func RestForward(r *http.Request, url string) (res *resty.Response, body interface{}, err error) {
	client := resty.New()

	var req *resty.Request
	if GetEnv().Retry.Count > 0 {
		retry := client.SetRetryCount(GetEnv().Retry.Count)

		if GetEnv().Retry.Delay > 0 {
			retry.SetRetryWaitTime(time.Duration(GetEnv().Retry.Delay) * time.Millisecond)
		}

		if GetEnv().Retry.DelayMax > 0 {
			retry.SetRetryMaxWaitTime(time.Duration(GetEnv().Retry.DelayMax) * time.Millisecond)
		}

		req = retry.R().EnableTrace()
	} else {
		req = client.R().EnableTrace()
	}
	req.Header = r.Header
	req.RawRequest = r
	req.Body = r.Body

	url += GetFullUrlQuery(r)

	switch r.Method {
	case http.MethodGet:
		res, err = req.Get(url)

		if err != nil {
			return nil, nil, err
		}
	case http.MethodPost:
		res, err = req.Post(url)

		if err != nil {
			return nil, nil, err
		}
	case http.MethodPut:
		res, err = req.Put(url)

		if err != nil {
			return nil, nil, err
		}
	case http.MethodDelete:
		res, err = req.Delete(url)

		if err != nil {
			return nil, nil, err
		}
	}

	contentType := res.Header().Get("Content-Type")

	if strings.Contains(contentType, "json") {
		body = map[string]interface{}{}
		err = json.Unmarshal(res.Body(), &body)

		if err != nil {
			return nil, nil, err
		}
	} else {
		body = res.RawBody()
	}

	return
}

func RestGo(method string, url string, headers http.Header, payload interface{}) (res *resty.Response, body interface{}, err error) {
	client := resty.New()

	var req *resty.Request
	if GetEnv().Retry.Count > 0 {
		retry := client.SetRetryCount(GetEnv().Retry.Count)

		if GetEnv().Retry.Delay > 0 {
			retry.SetRetryWaitTime(time.Duration(GetEnv().Retry.Delay) * time.Millisecond)
		}

		if GetEnv().Retry.DelayMax > 0 {
			retry.SetRetryMaxWaitTime(time.Duration(GetEnv().Retry.DelayMax) * time.Millisecond)
		}

		req = retry.R().EnableTrace()
	} else {
		req = client.R().EnableTrace()
	}

	if headers != nil {
		req.Header = headers
	}

	if payload != nil {
		req.SetBody(payload)
	}

	switch method {
	case http.MethodGet:
		res, err = req.Get(url)

		if err != nil {
			return nil, nil, err
		}
	case http.MethodPost:
		res, err = req.Post(url)

		if err != nil {
			return nil, nil, err
		}
	case http.MethodPut:
		res, err = req.Put(url)

		if err != nil {
			return nil, nil, err
		}
	case http.MethodDelete:
		res, err = req.Delete(url)

		if err != nil {
			return nil, nil, err
		}
	}

	contentType := res.Header().Get("Content-Type")

	if strings.Contains(contentType, "json") {
		body = map[string]interface{}{}
		err = json.Unmarshal(res.Body(), &body)

		if err != nil {
			return nil, nil, err
		}
	} else {
		body = res.RawBody()
	}

	return
}

func RestLog(r *http.Request, status int, logLocation string, logDescription string) (res *resty.Response, body interface{}, err error) {
	contentType := r.Header.Get("Content-Type")

	if !strings.Contains(contentType, "json") {
		r.Header.Set("Content-Type", "application/json")
	}

	extras := map[string]interface{}{}

	_, claims, _ := DecodeJwt(r)

	if claims.IdUser != "" {
		extras["idUser"] = claims.IdUser
	}

	if claims.IdCompany != "" {
		extras["idCompany"] = claims.IdCompany
	}

	newLog := models.LogModel{
		Status:      status,
		Location:    logLocation,
		Description: logDescription,
		ServiceName: GetEnv().ServiceName,
		Extras:      extras,
	}

	return RestGo(http.MethodPost, GetEnv().Address.LogService+"/create", r.Header, newLog)
}
