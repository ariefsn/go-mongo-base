package helper

import (
	"encoding/json"
	"go-mongo/global/types"
	"net/http"

	"github.com/fatih/structs"
	"github.com/go-chi/jwtauth/v5"
	"github.com/lestrrat-go/jwx/jwt"
)

var tokenAuth *jwtauth.JWTAuth

type Claims struct {
	IdUser    string           `json:"idUser"`
	Email     string           `json:"email"`
	IdCompany string           `json:"idCompany"`
	Role      types.UserRole   `json:"role"`
	Status    types.UserStatus `json:"status"`
}

func InitJwt() {
	secret := GetEnv().JwtSecret

	tokenAuth = jwtauth.New("HS256", []byte(secret), nil)
}

func EncodeJwt(claims Claims) (token jwt.Token, tokenString string, err error) {
	m := structs.Map(claims)

	return tokenAuth.Encode(m)
}

func DecodeJwt(r *http.Request) (token jwt.Token, claims Claims, err error) {
	token, claimsMap, err := jwtauth.FromContext(r.Context())
	claimsString, _ := json.Marshal(claimsMap)
	json.Unmarshal(claimsString, &claims)
	return
}

func TokenAuth() *jwtauth.JWTAuth {
	return tokenAuth
}

func Authenticator(next http.Handler) http.Handler {
	res := func(message string) map[string]interface{} {
		return map[string]interface{}{
			"success": false,
			"data":    nil,
			"message": message,
		}
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		token, _, err := jwtauth.FromContext(r.Context())

		if err != nil {
			Render().JSON(w, http.StatusUnauthorized, res(err.Error()))

			return
		}

		if token == nil || jwt.Validate(token) != nil {
			Render().JSON(w, http.StatusUnauthorized, res(http.StatusText(http.StatusUnauthorized)))
			return
		}

		// Token is authenticated, pass it through
		next.ServeHTTP(w, r)
	})
}
