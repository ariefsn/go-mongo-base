package helper

import (
	"os"
	"strconv"
	"strings"

	"github.com/joho/godotenv"
)

type Env struct {
	Host        string
	Port        string
	JwtSecret   string
	ServiceName string
	Db          struct {
		Host     string
		User     string
		Password string
		Name     string
		Port     string
	}
	Timezone string
	Cors     struct {
		AllowedOrigins []string
		AllowedMethods []string
		AllowedHeaders []string
	}
	Swagger struct {
		Protocol string
		Host     string
		Port     string
		BasePath string
	}
	Retry struct {
		Count    int
		Delay    int
		DelayMax int
	}
	Address struct {
		CompanyService     string
		UserService        string
		ProviderService    string
		GroupService       string
		ProductService     string
		TransactionService string
		StorageService     string
		LogService         string
	}
	Upload struct {
		Path    string
		MaxSize int
		Types   []string
		Group   string
	}
}

var env = new(Env)

func InitEnv(filenames ...string) error {
	err := godotenv.Load(filenames...)

	if err != nil {
		return err
	}

	env.Host = FromEnv("HOST", "0.0.0.0")
	env.Port = FromEnv("PORT", "4002")
	env.JwtSecret = FromEnv("JWT_SECRET", "")
	env.ServiceName = FromEnv("SERVICE_NAME", "Company Service")
	env.Db.Host = FromEnv("DB_HOST", "")
	env.Db.User = FromEnv("DB_USER", "")
	env.Db.Password = FromEnv("DB_PASSWORD", "")
	env.Db.Name = FromEnv("DB_NAME", "")
	env.Db.Port = FromEnv("DB_PORT", "27017")

	env.Timezone = FromEnv("TIMEZONE", "")

	env.Cors.AllowedOrigins = []string{"http://localhost:" + env.Port, "http://0.0.0.0:" + env.Port}

	if os.Getenv("CORS_ORIGINS") != "" {
		env.Cors.AllowedOrigins = append(env.Cors.AllowedOrigins, strings.Split(os.Getenv("CORS_ORIGINS"), ",")...)
	}

	env.Cors.AllowedMethods = []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}

	if os.Getenv("CORS_METHODS") != "" {
		env.Cors.AllowedOrigins = strings.Split(os.Getenv("CORS_METHODS"), ",")
	}

	env.Cors.AllowedHeaders = []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"}

	if os.Getenv("CORS_HEADERS") != "" {
		env.Cors.AllowedHeaders = strings.Split(os.Getenv("CORS_HEADERS"), ",")
	}

	// Swagger
	env.Swagger.Protocol = FromEnv("SWAGGER_PROTOCOL", "http://")
	env.Swagger.Host = FromEnv("SWAGGER_HOST", "localhost")
	env.Swagger.Port = FromEnv("SWAGGER_PORT", env.Port)
	env.Swagger.BasePath = FromEnv("SWAGGER_BASE_PATH", "")

	// Retry
	env.Retry.Count = 0
	if os.Getenv("RETRY_COUNT") != "" {
		env.Retry.Count, _ = strconv.Atoi(os.Getenv("RETRY_COUNT"))
	}

	env.Retry.Delay = 0
	if os.Getenv("RETRY_DELAY") != "" {
		env.Retry.Delay, _ = strconv.Atoi(os.Getenv("RETRY_DELAY"))
	}

	env.Retry.DelayMax = 0
	if os.Getenv("RETRY_DELAY_MAX") != "" {
		env.Retry.DelayMax, _ = strconv.Atoi(os.Getenv("RETRY_DELAY_MAX"))
	}

	// Address
	env.Address.CompanyService = FromEnv("ADDRESS_COMPANY_SERVICE", "http://localhost:4002")
	env.Address.UserService = FromEnv("ADDRESS_USER_SERVICE", "http://localhost:4003")
	env.Address.ProviderService = FromEnv("ADDRESS_PROVIDER_SERVICE", "http://localhost:4004")
	env.Address.GroupService = FromEnv("ADDRESS_GROUP_SERVICE", "http://localhost:4005")
	env.Address.ProductService = FromEnv("ADDRESS_PRODUCT_SERVICE", "http://localhost:4006")
	env.Address.TransactionService = FromEnv("ADDRESS_TRANSACTION_SERVICE", "http://localhost:4007")
	env.Address.StorageService = FromEnv("ADDRESS_STORAGE_SERVICE", "http://localhost:4008")
	env.Address.LogService = FromEnv("ADDRESS_LOG_SERVICE", "http://localhost:4009")

	// Upload
	env.Upload.Path = FromEnv("UPLOAD_PATH", "uploads")
	env.Upload.MaxSize = 1
	if os.Getenv("UPLOAD_MAX_SIZE") != "" {
		uploadMaxSize, _ := strconv.Atoi(os.Getenv("UPLOAD_MAX_SIZE"))

		env.Upload.MaxSize = uploadMaxSize * 1024 * 1024
	}
	env.Upload.Types = []string{
		"image/jpeg",
		"image/jpg",
		"image/gif",
		"image/png",
		"application/pdf",
	}
	if os.Getenv("UPLOAD_FILE_TYPES") != "" {
		env.Upload.Types = strings.Split(os.Getenv("UPLOAD_FILE_TYPES"), ",")
	}
	env.Upload.Group = FromEnv("UPLOAD_GROUP", "companies")

	return nil
}

func GetEnv() *Env {
	return env
}

func FromEnv(key, defaultValue string) string {
	val := os.Getenv(key)

	if val == "" {
		return defaultValue
	}

	return val
}
