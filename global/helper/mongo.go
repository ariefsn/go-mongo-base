package helper

import (
	"fmt"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type FilterOperator string

const (
	FoContains  FilterOperator = "$contains"
	FoStartWith                = "$startWith"
	FoEndWith                  = "$endWith"
	FoEq                       = "$eq"
	FoNe                       = "$ne"
	FoIn                       = "$in"
	FoNin                      = "$nin"
)

type MongoSortBy string

const (
	SortByAsc  MongoSortBy = "ASC"
	SortByDesc             = "DESC"
)

type MongoSort struct {
	SortField string
	SortBy    MongoSortBy
}

type MongoAggregate struct {
	Skip  *int
	Limit *int
	Sort  []MongoSort
	Match bson.D
}

func NewMongoAggregate() *MongoAggregate {
	a := new(MongoAggregate)

	return a
}

func (a *MongoAggregate) BuildPipe() mongo.Pipeline {
	pipe := mongo.Pipeline{}

	// build match
	if a.Match != nil && len(a.Match) > 0 {
		pipe = append(pipe, MongoMatch(a.Match))
	}

	// build sort
	if len(a.Sort) > 0 {
		pipe = append(pipe, bson.D{
			bson.E{
				Key:   "$sort",
				Value: MongoSorting(a.Sort...),
			},
		})
	}

	// build skip
	if a.Skip != nil && *a.Skip > -1 {
		pipe = append(pipe, MongoSkip(*a.Skip))
	}

	// build limit
	if a.Limit != nil && *a.Limit > -1 {
		if *a.Skip < 0 {
			pipe = append(pipe, MongoLimit(*a.Limit))
		} else {
			pipe = append(pipe, MongoLimit(*a.Limit+*a.Skip))
		}
	}

	return pipe
}

func MongoPipe(aggregate MongoAggregate) mongo.Pipeline {
	return aggregate.BuildPipe()
}

func MongoMatch(filter bson.D) bson.D {
	return bson.D{
		bson.E{
			Key:   "$match",
			Value: filter,
		},
	}
}

func MongoSorting(sort ...MongoSort) bson.D {
	s := bson.D{}

	for _, v := range sort {
		sortBy := 1

		if v.SortBy == SortByDesc {
			sortBy = -1
		}

		s = append(s, bson.E{
			Key:   v.SortField,
			Value: sortBy,
		})
	}

	return s
}

func MongoSkip(skip int) bson.D {
	return bson.D{
		bson.E{
			Key:   "$skip",
			Value: skip,
		},
	}
}

func MongoLimit(limit int) bson.D {
	return bson.D{
		bson.E{
			Key:   "$limit",
			Value: limit,
		},
	}
}

func MongoFilter(operator FilterOperator, field string, value interface{}) bson.D {
	switch operator {
	case FoEq, FoNe, FoIn, FoNin:
		return bson.D{
			bson.E{
				Key: field,
				Value: bson.D{
					bson.E{
						Key:   string(operator),
						Value: value,
					},
				},
			},
		}
	case FoContains, FoStartWith, FoEndWith:
		var regexPattern string
		regexOpt := "i"

		switch operator {
		case FoContains:
			regexPattern = fmt.Sprintf(".*%s.*", value)
		case FoStartWith:
			regexPattern = fmt.Sprintf(".*%s", value)
		case FoEndWith:
			regexPattern = fmt.Sprintf("%s.*", value)
		}

		return bson.D{
			bson.E{
				Key: field,
				Value: bson.D{
					bson.E{
						Key:   "$regex",
						Value: regexPattern,
					},
					bson.E{
						Key:   "$options",
						Value: regexOpt,
					},
				},
			},
		}
	default:
		return nil
	}
}

func MongoFilterM(operator FilterOperator, field string, value interface{}) bson.M {
	m, _ := ToBsonM(MongoFilter(operator, field, value))

	return m
}
