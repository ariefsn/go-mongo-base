package helper

import (
	"context"
	"time"
)

func NewContextWithTimeoutSecond(second int) (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Duration(second)*time.Second)
}

func NewContextWithTimeoutMillisecond(ms int) (context.Context, context.CancelFunc) {
	return context.WithTimeout(context.Background(), time.Duration(ms)*time.Millisecond)
}
