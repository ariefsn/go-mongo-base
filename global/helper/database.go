package helper

import (
	"context"
	"errors"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// var db *gorm.DB

// func InitDB() error {
// 	conn := fmt.Sprintf("%s:%s@tcp(%s)/%s?charset=utf8mb4&parseTime=True&loc=%s", GetEnv().Db.User, GetEnv().Db.Password, GetEnv().Db.Host, GetEnv().Db.Name, GetEnv().Db.Timezone)

// 	var err error

// 	db, err = gorm.Open(mysql.Open(conn), &gorm.Config{})

// 	return err
// }

// func GetDB() *gorm.DB {
// 	return db
// }

func DbClient() (*mongo.Client, error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	envDb := GetEnv().Db

	uri := fmt.Sprintf("mongodb://%s:%s@%s:%s/%s", envDb.User, envDb.Password, envDb.Host, envDb.Port, env.Db.Name)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(uri))

	if err != nil {
		return nil, errors.New("Init database failed. Error: " + err.Error())
	}

	return client, nil
}

func DBPing() error {
	client, err := DbClient()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	return client.Ping(ctx, readpref.Primary())
}

func DBInstance() (*mongo.Database, error) {
	client, err := DbClient()
	if err != nil {
		return nil, err
	}

	return client.Database(GetEnv().Db.Name), nil
}
