package types

type UserRole int
type UserStatus int

const (
	UserRoleRoot     UserRole = 0
	UserRoleAdmin             = 1
	UserRoleCustomer          = 2
)

const (
	UserStatusInactive UserStatus = 0
	UserStatusPending             = 1
	UserStatusActive              = 2
)
