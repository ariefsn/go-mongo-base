package swagger

type ResponseUser struct {
	Success bool   `json:"success" example:"true"`
	Data    Map    `json:"data" swaggertype:"object,string" example:"id:string,idCompany:string,firstName:string,lastName:string,email:string,phone:string,image:string,birthdate:datetime,isActive:boolean,isOwner:boolean,isAdmin:boolean,createdAt:datetime,updatedAt:datetime"`
	Message string `json:"message" example:""`
}

type ResponseUserList struct {
	Success bool `json:"success" example:"true"`
	Data    ListMap
	Message string `json:"message" example:""`
}
