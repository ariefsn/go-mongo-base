package swagger

type ResponseLog struct {
	Success bool   `json:"success" example:"true"`
	Data    Map    `json:"data" swaggertype:"object,string" example:"id:string,idCompany:string,idUser:string,serviceName:string,description:string,status:int,createdAt:datetime"`
	Message string `json:"message" example:"some error message"`
}

type ResponseLogList struct {
	Success bool `json:"success" example:"true"`
	Data    ListMap
	Message string `json:"message" example:""`
}
