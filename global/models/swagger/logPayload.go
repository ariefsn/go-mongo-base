package swagger

type PayloadLog struct {
	ServiceName string `json:"serviceName" example:"string"`
	Location    string `json:"location" example:"string"`
	Description string `json:"description" example:"string"`
	Status      int    `json:"status" example:"200"`
	Extras      Map    `json:"extras" swaggertype:"object,string" example:""`
}
