package swagger

type PayloadHero struct {
	RealName string `json:"realName" example:"string"`
	Name     string `json:"name" example:"string"`
	Type     string `json:"type" example:"string"`
	Group    string `json:"group" example:"string"`
}
