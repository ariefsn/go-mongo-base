package swagger

type PayloadUser struct {
	FirstName string `json:"firstName" example:"string"`
	LastName  string `json:"lastName" example:"string"`
	Email     string `json:"email" example:"string"`
	Password  string `json:"password" example:"string"`
	Phone     string `json:"phone" example:"string"`
}

type PayloadLogin struct {
	Email    string `json:"email" example:"string"`
	Password string `json:"password" example:"string"`
}
