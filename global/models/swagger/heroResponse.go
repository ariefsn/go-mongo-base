package swagger

type ResponseHero struct {
	Success bool   `json:"success" example:"true"`
	Data    Map    `json:"data" swaggertype:"object,string" example:"id:string,name:string,realName:string,group:string,type:string,createdAt:datetime,updatedAt:datetime"`
	Message string `json:"message" example:""`
}

type ResponseHeroList struct {
	Success bool `json:"success" example:"true"`
	Data    ListMap
	Message string `json:"message" example:""`
}
