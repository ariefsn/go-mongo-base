package controllers

import (
	"fmt"
	"go-mongo/global/helper"
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
)

type StorageController struct {
	BaseController
}

func NewStorageController() *StorageController {
	return new(StorageController)
}

func (c *StorageController) Register(r *chi.Mux) *chi.Mux {
	r.Get("/file/{group}/{filename}", c.File)

	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(helper.TokenAuth()))
		r.Use(helper.Authenticator)

		r.Post("/upload/{group}", c.Upload)
		r.Post("/upload/{group}/{filename}", c.UploadRename)
		r.Delete("/delete/{group}/{filename}", c.Delete)
	})

	return r
}

// Upload godoc
// @Summary Upload to storage
// @Description Upload file to storage
// @Tags Storage
// @Accept multipart/form-data
// @Produce json
// @Security Bearer
// @Param group path string true "Group storage"
// @Param file formData file true "Some file"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /upload/{group} [post]
// Handler for upload file to storage
func (c *StorageController) Upload(rw http.ResponseWriter, r *http.Request) {
	logLocation := "Storage/Upload"

	group := chi.URLParam(r, "group")
	rename := chi.URLParam(r, "filename")

	if group == "" {
		RenderError(rw, http.StatusInternalServerError, Msg.GroupStorageRequired)
		helper.RestLog(r, http.StatusInternalServerError, logLocation, Msg.GroupStorageRequired)
		return
	}

	fileName, err := helper.UploadFile(r, group, rename)

	if err != nil {
		RenderError(rw, http.StatusInternalServerError, err.Error())
		helper.RestLog(r, http.StatusInternalServerError, logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, fileName)
	helper.RestLog(r, http.StatusOK, logLocation, fmt.Sprintf("upload file with name %s/%s success", group, fileName))
}

// UploadRename godoc
// @Summary Upload and rename to storage
// @Description Upload and rename file to storage
// @Tags Storage
// @Accept multipart/form-data
// @Produce json
// @Security Bearer
// @Param group path string true "Group storage"
// @Param filename path string false "Rename file"
// @Param file formData file true "Some file"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /upload/{group}/{filename} [post]
// Handler for upload and rename file to storage
func (c *StorageController) UploadRename(rw http.ResponseWriter, r *http.Request) {
	logLocation := "Storage/UploadRename"

	group := chi.URLParam(r, "group")
	rename := chi.URLParam(r, "filename")

	if group == "" {
		RenderError(rw, http.StatusInternalServerError, Msg.GroupStorageRequired)
		helper.RestLog(r, http.StatusInternalServerError, logLocation, Msg.GroupStorageRequired)
		return
	}

	fileName, err := helper.UploadFile(r, group, rename)

	if err != nil {
		RenderError(rw, http.StatusInternalServerError, err.Error())
		helper.RestLog(r, http.StatusInternalServerError, logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, fileName)
	helper.RestLog(r, http.StatusOK, logLocation, fmt.Sprintf("upload and rename file with name %s/%s success", group, fileName))
}

// File godoc
// @Summary Get file from storage
// @Description View or download file from storage
// @Tags Storage
// @Accept */*
// @Produce */*
// @Param group path string true "Group storage"
// @Param filename path string true "File name"
// @Param download query boolean false "Prompt download"
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /file/{group}/{filename} [get]
// Handler for view or download file from storage
func (c *StorageController) File(rw http.ResponseWriter, r *http.Request) {
	group := chi.URLParam(r, "group")
	fileName := chi.URLParam(r, "filename")
	isDownload := strings.ToLower(r.URL.Query().Get("download")) == "true"

	if group == "" {
		RenderError(rw, http.StatusInternalServerError, Msg.GroupStorageRequired)
		return
	}

	if fileName == "" {
		RenderError(rw, http.StatusInternalServerError, Msg.FileNameRequired)
		return
	}

	file, path, err := helper.DownloadFile(r, group, fileName)

	if file != nil {
		defer file.Close()
	}

	if err != nil {
		RenderError(rw, http.StatusInternalServerError, err.Error())
		return
	}

	helper.AddContentDisposition(isDownload, rw, fileName)
	rw.WriteHeader(http.StatusOK)
	http.ServeFile(rw, r, path)
}

// Delete godoc
// @Summary Delete from storage
// @Description Delete file from storage
// @Tags Storage
// @Accept json
// @Produce json
// @Security Bearer
// @Param group path string true "Group storage"
// @Param filename path string true "File name"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /delete/{group}/{filename} [delete]
// Handler for delete file from storage
func (c *StorageController) Delete(rw http.ResponseWriter, r *http.Request) {
	logLocation := "Storage/Delete"

	group := chi.URLParam(r, "group")
	fileName := chi.URLParam(r, "filename")

	if group == "" {
		RenderError(rw, http.StatusInternalServerError, Msg.GroupStorageRequired)
		helper.RestLog(r, http.StatusInternalServerError, logLocation, Msg.GroupStorageRequired)
		return
	}

	if fileName == "" {
		RenderError(rw, http.StatusInternalServerError, Msg.FileNameRequired)
		helper.RestLog(r, http.StatusInternalServerError, logLocation, Msg.FileNameRequired)
		return
	}

	deleted, err := helper.RemoveFile(r, group, fileName)

	if err != nil {
		RenderError(rw, http.StatusInternalServerError, err.Error())
		helper.RestLog(r, http.StatusInternalServerError, logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, deleted)
	helper.RestLog(r, http.StatusOK, logLocation, fmt.Sprintf("Delete file with name %s/%s success", group, fileName))
}
