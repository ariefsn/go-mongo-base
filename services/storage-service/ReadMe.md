# STORAGE SERVICE

> Storage Service.

## Features

- Upload file
- Upload and rename file
- Download file

## How To

1. Copy `.env.example` to `.env`
2. Setup configuration `.env`
3. `go run main.go`
