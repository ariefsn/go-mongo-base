package repositories_test

import (
	"errors"
	"go-mongo/global/helper"
	"go-mongo/services/api-service/models"
	"go-mongo/services/api-service/repositories"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/integration/mtest"
)

var heroes = []models.HeroModel{
	{
		Id:        primitive.NewObjectID(),
		RealName:  "Tony Stark",
		Name:      "Ironman",
		Type:      models.Good,
		Group:     models.Marvel,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Id:        primitive.NewObjectID(),
		RealName:  "Clint Barton",
		Name:      "Hawkeye",
		Type:      models.Good,
		Group:     models.Marvel,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Id:        primitive.NewObjectID(),
		RealName:  "Bruce Wayne",
		Name:      "Batman",
		Type:      models.Good,
		Group:     models.Marvel,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
}

func toBsonD(d models.HeroModel) bson.D {
	return bson.D{
		bson.E{Key: "_id", Value: d.Id},
		bson.E{Key: "realName", Value: d.RealName},
		bson.E{Key: "name", Value: d.Name},
		bson.E{Key: "type", Value: d.Type},
		bson.E{Key: "group", Value: d.Group},
		bson.E{Key: "createdAt", Value: d.CreatedAt},
		bson.E{Key: "updatedAt", Value: d.UpdatedAt},
	}
}

func TestGetById(t *testing.T) {
	opt := mtest.NewOptions()
	opt.ClientType(mtest.Mock)

	mt := mtest.New(t, opt)
	defer mt.Close()

	hero := heroes[0]

	testCases := []struct {
		Name             string
		FilterId         string
		Data             bson.D
		ExpectedResponse *models.HeroModel
		ExpectedError    error
	}{
		{
			Name:             "Found",
			FilterId:         hero.Id.Hex(),
			Data:             toBsonD(hero),
			ExpectedError:    nil,
			ExpectedResponse: &hero,
		},
		{
			Name:             "Not Found",
			FilterId:         primitive.NewObjectID().Hex(),
			ExpectedError:    mongo.ErrNoDocuments,
			ExpectedResponse: nil,
		},
		{
			Name:             "Invalid Id",
			FilterId:         "some id",
			ExpectedError:    mongo.ErrNoDocuments,
			ExpectedResponse: nil,
		},
	}

	for _, tc := range testCases {
		mt.Run(tc.Name, func(mt *mtest.T) {

			if tc.Data != nil {
				mt.AddMockResponses(mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, tc.Data))
			} else {
				mt.AddMockResponses(mtest.CreateCommandErrorResponse(mtest.CommandError{Message: mongo.ErrNoDocuments.Error()}))
			}

			repo := repositories.NewHeroRepository(mt.DB)

			res, err := repo.GetById(tc.FilterId)

			if tc.ExpectedError == nil {
				assert.Nil(t, err)
				assert.Equal(t, res.RealName, tc.Data[1].Value)
			} else {
				assert.NotNil(t, err)
				assert.Nil(t, res)
			}
		})
	}
}

func TestGetByField(t *testing.T) {
	opt := mtest.NewOptions()
	opt.ClientType(mtest.Mock)

	mt := mtest.New(t, opt)
	defer mt.Close()

	hero := heroes[0]

	testCases := []struct {
		Name             string
		FilterName       string
		Data             bson.D
		ExpectedResponse *models.HeroModel
		ExpectedError    error
	}{
		{
			Name:             "Found",
			FilterName:       "Iron",
			Data:             toBsonD(hero),
			ExpectedError:    nil,
			ExpectedResponse: &hero,
		},
		{
			Name:             "Not Found",
			FilterName:       "Superman",
			ExpectedError:    mongo.ErrNoDocuments,
			ExpectedResponse: nil,
		},
	}

	for _, tc := range testCases {
		mt.Run(tc.Name, func(mt *mtest.T) {

			if tc.Data != nil {
				mt.AddMockResponses(mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, tc.Data))
			} else {
				mt.AddMockResponses(mtest.CreateCommandErrorResponse(mtest.CommandError{Message: mongo.ErrNoDocuments.Error()}))
			}

			repo := repositories.NewHeroRepository(mt.DB)

			res, err := repo.GetByField(helper.MongoFilter(helper.FoEq, "name", tc.FilterName))

			if tc.ExpectedError == nil {
				assert.Nil(t, err)
				assert.Equal(t, res.RealName, tc.Data[1].Value)
			} else {
				assert.NotNil(t, err)
				assert.Nil(t, res)
			}
		})
	}
}

func TestInsert(t *testing.T) {
	opt := mtest.NewOptions()
	opt.ClientType(mtest.Mock)

	mt := mtest.New(t, opt)
	defer mt.Close()

	hero := heroes[0]

	testCases := []struct {
		Name             string
		Data             *models.HeroModel
		ExpectedResponse interface{}
		ExpectedError    error
	}{
		{
			Name:             "Success Insert",
			Data:             &hero,
			ExpectedError:    nil,
			ExpectedResponse: hero.Id,
		},
		{
			Name:             "Error Duplicate",
			Data:             &hero,
			ExpectedError:    errors.New(helper.NewMessage().Duplicate),
			ExpectedResponse: nil,
		},
	}

	for _, tc := range testCases {
		mt.Run(tc.Name, func(mt *mtest.T) {

			if tc.ExpectedError == nil {
				mt.AddMockResponses(mtest.CreateSuccessResponse())
				mt.AddMockResponses(mtest.CreateSuccessResponse())
			} else {
				mt.AddMockResponses(mtest.CreateCursorResponse(1, "foo.bar", mtest.FirstBatch, toBsonD(*tc.Data)))
				mt.AddMockResponses(mtest.CreateWriteErrorsResponse(mtest.WriteError{
					Message: "duplicate",
				}))
			}

			repo := repositories.NewHeroRepository(mt.DB)

			res, err := repo.Insert(tc.Data)

			if tc.ExpectedError == nil {
				assert.Nil(t, err)
				assert.Equal(t, tc.Data.Id, res)
			} else {
				assert.NotNil(t, err)
				assert.Nil(t, res)
			}
		})
	}
}

func TestUpdate(t *testing.T) {
	opt := mtest.NewOptions()
	opt.ClientType(mtest.Mock)

	mt := mtest.New(t, opt)
	defer mt.Close()

	hero := heroes[0]

	testCases := []struct {
		Name             string
		Data             *models.HeroModel
		ExpectedResponse interface{}
		ExpectedError    error
	}{
		{
			Name:             "Success Update",
			Data:             &hero,
			ExpectedError:    nil,
			ExpectedResponse: hero.Id,
		},
		{
			Name:             "Data Not Found",
			Data:             &hero,
			ExpectedError:    errors.New(helper.NewMessage().DataNotFound),
			ExpectedResponse: nil,
		},
	}

	for _, tc := range testCases {
		mt.Run(tc.Name, func(mt *mtest.T) {

			if tc.ExpectedError == nil {
				mt.AddMockResponses(bson.D{
					bson.E{
						Key:   "ok",
						Value: 1,
					},
					bson.E{
						Key:   "value",
						Value: toBsonD(hero),
					},
				})
			} else {
				if tc.ExpectedError.Error() == helper.NewMessage().DataNotFound {
					mt.AddMockResponses(bson.D{
						bson.E{
							Key:   "ok",
							Value: 1,
						},
					})
				} else {
					mt.AddMockResponses(mtest.CreateWriteErrorsResponse(mtest.WriteError{
						Message: "duplicate",
					}))
				}
			}

			repo := repositories.NewHeroRepository(mt.DB)

			res, err := repo.Update(helper.MongoFilter(helper.FoEq, "_id", tc.Data.Id), *tc.Data)

			if tc.ExpectedError == nil {
				assert.Nil(t, err)
				assert.Equal(t, tc.Data.Id, res)
			} else {
				assert.NotNil(t, err)
				assert.Nil(t, res)
			}
		})
	}
}

func TestDelete(t *testing.T) {
	opt := mtest.NewOptions()
	opt.ClientType(mtest.Mock)

	mt := mtest.New(t, opt)
	defer mt.Close()

	hero := heroes[0]

	testCases := []struct {
		Name             string
		FilterId         string
		ExpectedResponse interface{}
		ExpectedError    error
	}{
		{
			Name:             "Success Delete",
			FilterId:         hero.Id.Hex(),
			ExpectedError:    nil,
			ExpectedResponse: hero.Id,
		},
		{
			Name:             "Data Not Found",
			FilterId:         hero.Id.Hex(),
			ExpectedError:    errors.New(helper.NewMessage().DataNotFound),
			ExpectedResponse: nil,
		},
	}

	for _, tc := range testCases {
		mt.Run(tc.Name, func(mt *mtest.T) {

			if tc.ExpectedError == nil {
				mt.AddMockResponses(bson.D{
					bson.E{
						Key:   "ok",
						Value: 1,
					},
					bson.E{
						Key:   "value",
						Value: toBsonD(hero),
					},
				})
			} else {
				if tc.ExpectedError.Error() == helper.NewMessage().DataNotFound {
					mt.AddMockResponses(bson.D{
						bson.E{
							Key:   "ok",
							Value: 1,
						},
					})
				} else {
					mt.AddMockResponses(mtest.CreateWriteErrorsResponse(mtest.WriteError{
						Message: "duplicate",
					}))
				}
			}

			repo := repositories.NewHeroRepository(mt.DB)

			res, err := repo.Delete(tc.FilterId)

			if tc.ExpectedError == nil {
				assert.Nil(t, err)
				assert.Equal(t, tc.FilterId, res)
			} else {
				assert.NotNil(t, err)
				assert.Nil(t, res)
			}
		})
	}
}

func TestAll(t *testing.T) {
	opt := mtest.NewOptions()
	opt.ClientType(mtest.Mock)

	mt := mtest.New(t, opt)
	defer mt.Close()

	testCases := []struct {
		Name             string
		ExpectedResponse interface{}
		ExpectedError    error
	}{
		{
			Name:             "Success",
			ExpectedError:    nil,
			ExpectedResponse: len(heroes),
		},
	}

	for _, tc := range testCases {
		mt.Run(tc.Name, func(mt *mtest.T) {

			if tc.ExpectedError == nil {
				mt.AddMockResponses(bson.D{
					bson.E{
						Key:   "ok",
						Value: 1,
					},
					bson.E{
						Key:   "count",
						Value: len(heroes),
					},
				})

				mockResponse := []bson.D{}

				for i, h := range heroes {
					batch := mtest.NextBatch

					if i == 0 {
						batch = mtest.FirstBatch
					}

					mockResponse = append(mockResponse, mtest.CreateCursorResponse(1, "foo.bar", batch, toBsonD(h)))
				}

				mockResponse = append(mockResponse, mtest.CreateCursorResponse(0, "foo.bar", mtest.NextBatch))

				mt.AddMockResponses(mockResponse...)
				for _, r := range mockResponse {
					mt.AddMockResponses(r)
				}
			}

			repo := repositories.NewHeroRepository(mt.DB)

			res, _, err := repo.All(0, 10, bson.D{})

			if tc.ExpectedError == nil {
				assert.Nil(t, err)
				// assert.Equal(t, tc.ExpectedResponse, count)
				assert.Equal(t, tc.ExpectedResponse, len(res))
			} else {
				assert.NotNil(t, err)
				assert.Nil(t, res)
			}
		})
	}
}
