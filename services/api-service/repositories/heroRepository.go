package repositories

import (
	"context"
	"errors"
	"go-mongo/global/helper"
	"go-mongo/services/api-service/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

type HeroRepository interface {
	GetById(id string) (*models.HeroModel, error)
	GetByField(filter bson.D) (*models.HeroModel, error)
	All(skip, limit int, filter bson.D) ([]models.HeroModel, int64, error)
	Insert(data *models.HeroModel) (interface{}, error)
	Update(filter bson.D, data models.HeroModel) (interface{}, error)
	Delete(id string) (interface{}, error)
}

type heroRepository struct {
	db  *mongo.Database
	msg *helper.Message
}

func NewHeroRepository(dbInstance *mongo.Database) HeroRepository {
	r := new(heroRepository)
	r.db = dbInstance
	r.msg = helper.NewMessage()

	return r
}

func (r *heroRepository) findOne(filter bson.M) (*models.HeroModel, error) {
	coll := r.db.Collection(models.NewHeroModel().TableName())

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var res *models.HeroModel

	err := coll.FindOne(ctx, filter).Decode(&res)

	if err != nil {
		if err.Error() == mongo.ErrNoDocuments.Error() {
			return nil, errors.New(r.msg.DataNotFound)
		}
		return nil, err
	}

	return res, nil
}

func (r *heroRepository) GetById(id string) (*models.HeroModel, error) {
	objId, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return nil, err
	}

	filter := bson.M{"_id": objId}

	return r.findOne(filter)
}

func (r *heroRepository) GetByField(filter bson.D) (*models.HeroModel, error) {
	filterM, _ := helper.ToBsonM(filter)

	return r.findOne(filterM)
}

func (r *heroRepository) All(skip, limit int, filter bson.D) ([]models.HeroModel, int64, error) {
	res := []models.HeroModel{}

	ctx, cancel := helper.NewContextWithTimeoutSecond(15)
	defer cancel()

	coll := r.db.Collection(models.NewHeroModel().TableName())

	count, err := coll.CountDocuments(ctx, filter) // .EstimatedDocumentCount(ctx, &options.EstimatedDocumentCountOptions{})

	if err != nil && r.db.Name() != "test" {
		return res, 0, err
	}

	pipe := helper.MongoPipe(helper.MongoAggregate{
		Match: filter,
		Skip:  &skip,
		Limit: &limit,
	})

	cur, err := coll.Aggregate(ctx, pipe)

	if err != nil {
		return res, 0, err
	}

	defer cur.Close(ctx)

	for cur.Next(ctx) {
		var result models.HeroModel

		err = cur.Decode(&result)
		if err != nil {
			break
		}

		res = append(res, result)
	}

	if err != nil {
		return []models.HeroModel{}, 0, err
	}

	return res, count, nil
}

func (r *heroRepository) Insert(data *models.HeroModel) (interface{}, error) {
	// check
	check, _ := r.GetByField(helper.MongoFilter(helper.FoEq, "name", data.Name))
	// if err != nil {
	// 	return nil, err
	// }

	if check != nil {
		if check.Name == data.Name {
			return nil, errors.New(r.msg.Duplicate)
		}
	}

	coll := r.db.Collection(data.TableName())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	data.Id = primitive.NewObjectID()
	data.CreatedAt = helper.GetTimeLocale().UTC()
	data.UpdatedAt = data.CreatedAt

	res, err := coll.InsertOne(ctx, data)

	if err != nil {
		return nil, err
	}

	return res.InsertedID, nil
}

func (r *heroRepository) Update(filter bson.D, data models.HeroModel) (interface{}, error) {
	coll := r.db.Collection(data.TableName())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	data.UpdateTime()

	res := coll.FindOneAndUpdate(ctx, filter, bson.D{
		bson.E{
			Key:   "$set",
			Value: data,
		},
	})

	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return nil, errors.New(r.msg.DataNotFound)
		}

		return nil, res.Err()
	}

	return data.Id, nil
}

func (r *heroRepository) Delete(id string) (interface{}, error) {
	coll := r.db.Collection(models.NewHeroModel().TableName())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	objId, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return nil, err
	}

	res := coll.FindOneAndDelete(ctx, helper.MongoFilter(helper.FoEq, "_id", objId))

	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			return nil, errors.New(r.msg.DataNotFound)
		}

		return nil, res.Err()
	}

	return id, nil
}
