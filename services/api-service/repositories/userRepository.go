package repositories

import (
	"context"
	"errors"
	"go-mongo/global/helper"
	"go-mongo/services/api-service/models"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type UserRepository interface {
	GetById(id string) (*models.UserModel, error)
	GetByEmail(email string) (*models.UserModel, error)
	Insert(data *models.UserModel) (interface{}, error)
	Update(filter, data bson.D) (interface{}, error)
	InitPasswordSuperUser() (interface{}, error)
	GenerateToken(email, password string) (string, error)
}

type userRepository struct {
	db  *mongo.Database
	msg *helper.Message
}

func NewUserRepository(dbInstance *mongo.Database) UserRepository {
	r := new(userRepository)
	r.db = dbInstance
	r.msg = helper.NewMessage()

	return r
}

func (r *userRepository) findOne(filter bson.M) (*models.UserModel, error) {
	coll := r.db.Collection(models.NewUserModel().TableName())

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	var res *models.UserModel

	err := coll.FindOne(ctx, filter).Decode(&res)

	if err == mongo.ErrNoDocuments {
		return nil, errors.New(r.msg.DataNotFound)
	} else if err != nil {
		return nil, err
	}

	return res, nil
}

func (r *userRepository) GetById(id string) (*models.UserModel, error) {
	filter := bson.M{"_id": id}

	return r.findOne(filter)
}

func (r *userRepository) GetByEmail(email string) (*models.UserModel, error) {
	filter := bson.M{"email": email}

	return r.findOne(filter)
}

func (r *userRepository) Insert(data *models.UserModel) (interface{}, error) {
	// check
	check, err := r.GetByEmail(data.Email)
	if err != nil {
		return nil, err
	}

	if check.Email == data.Email {
		return nil, errors.New(r.msg.UserEmailRegistered)
	}

	coll := r.db.Collection(data.TableName())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	res, err := coll.InsertOne(ctx, data)

	if err != nil {
		return nil, err
	}

	return res.InsertedID, nil
}

func (r *userRepository) Update(filter, data bson.D) (interface{}, error) {
	coll := r.db.Collection(models.NewUserModel().TableName())
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()

	res, err := coll.UpdateOne(ctx, filter, data)

	if err != nil {
		return nil, err
	}

	return res.UpsertedID, nil
}

func (r *userRepository) InitPasswordSuperUser() (interface{}, error) {
	res, err := r.GetByEmail("root@mail.com")

	if err != nil {
		return nil, err
	}

	if res.Password != "" {
		return res.Id, nil
	}

	res.Password, err = helper.HashPassword("Password.123")

	if err != nil {
		return nil, err
	}

	filter := bson.D{
		bson.E{
			Key:   "_id",
			Value: res.Id,
		},
	}

	updateData := bson.D{
		bson.E{
			Key: "$set",
			Value: bson.D{
				bson.E{
					Key:   "password",
					Value: res.Password,
				},
			},
		},
	}

	return r.Update(filter, updateData)
}

func (r *userRepository) GenerateToken(email, password string) (string, error) {
	res, err := r.GetByEmail(email)

	if err != nil {
		return "", err
	}

	if !helper.CheckPasswordHash(password, res.Password) {
		return "", errors.New(r.msg.InvalidCredentials)
	}

	_, tokenString, err := helper.EncodeJwt(helper.Claims{
		IdUser: res.Id.String(),
		Email:  res.Email,
	})

	if err != nil {
		return "", err
	}

	return tokenString, nil
}
