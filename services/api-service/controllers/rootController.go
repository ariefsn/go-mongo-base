package controllers

import (
	"fmt"
	"go-mongo/global/helper"
	"net/http"

	"github.com/go-chi/chi/v5"
)

type RootController struct {
	BaseController
}

func NewRootController() *RootController {
	return new(RootController)
}

func (c *RootController) Register(r *chi.Mux) *chi.Mux {
	r.Get("/", c.Ping)

	return r
}

// Ping godoc
// @Summary Ping api gateway
// @Description Ping api gateway
// @Tags Root
// @Accept json
// @Produce json
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router / [get]
// Handler for ping api gateway
func (c *RootController) Ping(rw http.ResponseWriter, r *http.Request) {
	RenderSuccess(rw, http.StatusOK, fmt.Sprintf("%v is running", helper.GetEnv().ServiceName))
}
