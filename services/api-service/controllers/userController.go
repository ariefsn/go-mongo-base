package controllers

import (
	"go-mongo/global/helper"
	"go-mongo/global/models/swagger"
	"go-mongo/services/api-service/models"
	"go-mongo/services/api-service/repositories"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
)

type UserController interface {
	Register(r *chi.Mux) *chi.Mux
	Login(rw http.ResponseWriter, r *http.Request)
	Profile(rw http.ResponseWriter, r *http.Request)
	Create(rw http.ResponseWriter, r *http.Request)
	CreateAdmin(rw http.ResponseWriter, r *http.Request)
}

type userController struct {
	BaseController
	repo repositories.UserRepository
}

func NewUserController(repo repositories.UserRepository) UserController {
	c := new(userController)
	c.repo = repo

	return c
}

func (c *userController) Register(r *chi.Mux) *chi.Mux {

	r.Route("/user", func(r chi.Router) {
		r.Post("/login", c.Login)

		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verifier(helper.TokenAuth()))
			r.Use(helper.Authenticator)

			r.Get("/me", c.Profile)
			r.Post("/create", c.Create)
			r.Post("/create-admin", c.CreateAdmin)
		})
	})

	return r
}

// Login godoc
// @Summary Login user
// @Description Login user and generate token
// @Tags User
// @Accept json
// @Produce json
// @Param body body swagger.PayloadLogin true "Payload"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /user/login [post]
// Handler for login user and generate token
func (c *userController) Login(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "User/Login"

	// res, body, err := helper.RestForward(r, helper.GetEnv().Address.UserService+"/login")
	payload := swagger.PayloadLogin{}

	if err := helper.ParsePayload(r, &payload); err != nil {
		RenderError(rw, http.StatusUnprocessableEntity, err.Error())
		return
	}

	res, err := c.repo.GenerateToken(payload.Email, payload.Password)

	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		// helper.RestLog(r, res.StatusCode(), logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, res)
	// helper.RestLog(r, http.StatusOK, logLocation, "login success")
}

// Profile godoc
// @Summary Profile user
// @Description Get user profile
// @Tags User
// @Accept json
// @Produce json
// @Security Bearer
// @Success 200 {object} swagger.ResponseUser
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /user/me [get]
// Handler for get user profile
func (c *userController) Profile(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "User/Profile"

	_, claims, err := helper.DecodeJwt(r)
	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		return
	}

	res, err := c.repo.GetByEmail(claims.Email)
	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		return
	}

	RenderForward(rw, http.StatusOK, helper.HideFields(res, "password"))
	// helper.RestLog(r, http.StatusOK, logLocation, "get profile success")
}

// Create godoc
// @Summary Create user
// @Description Create a new user
// @Tags User
// @Accept json
// @Produce json
// @Security Bearer
// @Param body body swagger.PayloadUser true "Payload"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /user/create [post]
// Handler for create new user
func (c *userController) Create(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "User/Create"

	payload := models.UserModel{}
	if err := helper.ParsePayload(r, &payload); err != nil {
		RenderError(rw, http.StatusUnprocessableEntity, err.Error())
		return
	}

	res, err := c.repo.Insert(&payload)
	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
	}

	RenderForward(rw, http.StatusOK, res)
	// helper.RestLog(r, res.StatusCode(), logLocation, fmt.Sprintf("create user with id %s success", body.(map[string]interface{})["data"]))
}

// CreateAdmin godoc
// @Summary Create user admin
// @Description Create a new user admin
// @Tags User
// @Accept json
// @Produce json
// @Security Bearer
// @Param body body swagger.PayloadUser true "Payload"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /user/create-admin [post]
// Handler for create new user admin
func (c *userController) CreateAdmin(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "User/CreateAdmin"

	res, body, err := helper.RestForward(r, helper.GetEnv().Address.UserService+"/login")

	if err != nil {
		RenderError(rw, res.StatusCode(), err.Error())
		// helper.RestLog(r, res.StatusCode(), logLocation, err.Error())
		return
	}

	if body == nil {
		RenderError(rw, http.StatusInternalServerError, Msg.EmptyResponse)
		// helper.RestLog(r, res.StatusCode(), logLocation, Msg.EmptyResponse)
		return
	}

	RenderForward(rw, res.StatusCode(), body)
	// helper.RestLog(r, res.StatusCode(), logLocation, fmt.Sprintf("create user with id %s success", body.(map[string]interface{})["data"]))
}
