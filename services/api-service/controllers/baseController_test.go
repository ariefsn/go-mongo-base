package controllers_test

import (
	"bytes"
	"encoding/json"
	"go-mongo/global/helper"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"go.mongodb.org/mongo-driver/bson"
)

const jwtTest = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJFbWFpbCI6InJvb3RAbWFpbC5jb20iLCJJZENvbXBhbnkiOiIiLCJJZFVzZXIiOiJPYmplY3RJRChcIjYyMWY1ODMxNTdhODlhM2VkOTg4MzIxNlwiKSIsIlJvbGUiOjAsIlN0YXR1cyI6MH0.EyUT-lfsO2pg0Tna3HN2sxMbEZ2pj2W4LkkokkOkNgs"

func prepareTest() {
	helper.InitEnv("../.env")
	helper.InitJwt()
	helper.InitRender()
}

func testRequest(t *testing.T, ts *httptest.Server, method, path string, header bson.M, body interface{}) (*http.Response, string) {
	data, _ := json.Marshal(body)

	reader := bytes.NewReader(data)

	req, err := http.NewRequest(method, ts.URL+path, reader)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}

	for k, v := range header {
		req.Header.Add(k, v.(string))
	}

	resp, err := http.DefaultClient.Do(req)

	if err != nil {
		t.Fatal(err)
		return nil, ""
	}

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
		return nil, ""
	}
	defer resp.Body.Close()

	return resp, string(respBody)
}

// func TestResponseBinary(t *testing.T) {
// 	testCases := []struct{
// 		Name string
// 		Response interface{}
// 		ExpectedResponse interface{}
// 		Code int
// 		ExpectedCode int
// 	}{
// 		{
// 			Name: "Success",
// 			Code: http.StatusOK,
// 			ExpectedCode: http.StatusOK,
// 			Response: []byte("Hello"),
// 			ExpectedResponse: []byte("Hello"),
// 		},
// 		{
// 			Name: "Failed",
// 			Code: http.StatusOK,
// 			ExpectedCode: http.StatusOK,
// 			Response: []byte("Hello"),
// 			ExpectedResponse: []byte("World"),
// 		},
// 	}

// 	for _, tc := range testCases {
// 		controllers.ResponseBinary()
// 	}
// }
