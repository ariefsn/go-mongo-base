package controllers_test

import (
	"errors"
	"go-mongo/global/helper"
	"go-mongo/services/api-service/models"
	"go-mongo/services/api-service/repositories"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

var heroes = []models.HeroModel{
	{
		Id:        primitive.NewObjectID(),
		RealName:  "Tony Stark",
		Name:      "Ironman",
		Type:      models.Good,
		Group:     models.Marvel,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Id:        primitive.NewObjectID(),
		RealName:  "Clint Barton",
		Name:      "Hawkeye",
		Type:      models.Good,
		Group:     models.Marvel,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
	{
		Id:        primitive.NewObjectID(),
		RealName:  "Bruce Wayne",
		Name:      "Batman",
		Type:      models.Good,
		Group:     models.Marvel,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	},
}

// Mock Repository for Controller
type heroRepositoryMock struct {
	db  *mongo.Database
	msg *helper.Message
}

func NewHeroRepositoryMock(dbInstance *mongo.Database) repositories.HeroRepository {
	r := new(heroRepositoryMock)
	r.db = dbInstance
	r.msg = helper.NewMessage()

	return r
}

func (r *heroRepositoryMock) findOne(filter bson.M) (*models.HeroModel, error) {
	switch filter["status"] {
	case r.msg.DataNotFound:
		return nil, errors.New(r.msg.DataNotFound)
	}

	return &heroes[0], nil
}

func (r *heroRepositoryMock) GetById(id string) (*models.HeroModel, error) {
	filter := bson.M{"status": id}

	return r.findOne(filter)
}

func (r *heroRepositoryMock) GetByField(filter bson.D) (*models.HeroModel, error) {
	filterM, _ := helper.ToBsonM(filter)

	return r.findOne(filterM)
}

func (r *heroRepositoryMock) All(skip, limit int, filter bson.D) ([]models.HeroModel, int64, error) {
	res := []models.HeroModel{}

	if skip == 0 && limit == 0 {
		return res, 0, errors.New(r.msg.DataNotFound)
	}

	res = heroes

	return res, int64(len(res)), nil
}

func (r *heroRepositoryMock) Insert(data *models.HeroModel) (interface{}, error) {
	switch data.Name {
	case r.msg.DataNotFound:
		return nil, errors.New(r.msg.DataNotFound)
	case r.msg.Duplicate:
		return nil, errors.New(r.msg.Duplicate)
	}

	return primitive.NewObjectID(), nil
}

func (r *heroRepositoryMock) Update(filter bson.D, data models.HeroModel) (interface{}, error) {
	switch data.Name {
	case r.msg.DataNotFound:
		return nil, errors.New(r.msg.DataNotFound)
	case r.msg.Duplicate:
		return nil, errors.New(r.msg.Duplicate)
	}

	return primitive.NewObjectID(), nil
}

func (r *heroRepositoryMock) Delete(id string) (interface{}, error) {
	switch id {
	case r.msg.DataNotFound:
		return nil, errors.New(r.msg.DataNotFound)
	case r.msg.Duplicate:
		return nil, errors.New(r.msg.Duplicate)
	}

	return primitive.NewObjectID(), nil
}
