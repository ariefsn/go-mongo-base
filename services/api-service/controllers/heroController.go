package controllers

import (
	"go-mongo/global/helper"
	"go-mongo/services/api-service/models"
	"go-mongo/services/api-service/repositories"
	"net/http"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/jwtauth/v5"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type HeroController interface {
	Register(r *chi.Mux) *chi.Mux
	Get(rw http.ResponseWriter, r *http.Request)
	Gets(rw http.ResponseWriter, r *http.Request)
	Put(rw http.ResponseWriter, r *http.Request)
	Post(rw http.ResponseWriter, r *http.Request)
	Delete(rw http.ResponseWriter, r *http.Request)
}

type heroController struct {
	BaseController
	repo repositories.HeroRepository
}

func NewHeroController(repo repositories.HeroRepository) HeroController {
	c := new(heroController)
	c.repo = repo

	return c
}

func (c *heroController) Register(r *chi.Mux) *chi.Mux {

	r.Route("/hero", func(r chi.Router) {
		r.Group(func(r chi.Router) {
			r.Use(jwtauth.Verifier(helper.TokenAuth()))
			r.Use(helper.Authenticator)

			r.Post("/", c.Post)
			r.Get("/", c.Gets)
			r.Get("/{id}", c.Get)
			r.Put("/{id}", c.Put)
			r.Delete("/{id}", c.Delete)
		})
	})

	return r
}

// Gets godoc
// @Summary List hero
// @Description Get hero list
// @Tags Hero
// @Accept json
// @Produce json
// @Security Bearer
// @Param name query string false "Name"
// @Param group query string false "Group"
// @Param skip query integer false "Skip"
// @Param limit query integer false "Limit"
// @Param orders query string false "Orders"
// @Success 200 {object} swagger.ResponseHeroList
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /hero [get]
// Handler for get hero list
func (c *heroController) Gets(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "Hero/Gets"

	skip, limit := helper.GetSkipLimit(r)

	var filters []bson.D

	name := helper.GetUrlQuery(r, "name")
	group := helper.GetUrlQuery(r, "group")

	if name != "" {
		filters = append(filters, helper.MongoFilter(helper.FoContains, "name", name))
	}

	if group != "" {
		filters = append(filters, helper.MongoFilter(helper.FoEq, "group", group))
	}

	filter := bson.D{}

	if len(filters) > 0 {
		filter = bson.D{
			bson.E{
				Key:   "$or",
				Value: filters,
			},
		}
	}

	res, count, err := c.repo.All(skip, limit, filter)
	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		return
	}

	RenderSuccessList(rw, http.StatusOK, res, int(count))
	// helper.RestLog(r, http.StatusOK, logLocation, "get profile success")
}

// Get godoc
// @Summary Detail hero
// @Description Detail hero
// @Tags Hero
// @Accept json
// @Produce json
// @Security Bearer
// @Param id path string true "Hero Id"
// @Success 200 {object} swagger.ResponseHero
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /hero/{id} [get]
// Handler for detail hero
func (c *heroController) Get(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "Hero/Get"

	id := chi.URLParam(r, "id")

	res, err := c.repo.GetById(id)

	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		// helper.RestLog(r, res.StatusCode(), logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, res)
	// helper.RestLog(r, http.StatusOK, logLocation, "login success")
}

// Put godoc
// @Summary Update hero
// @Description Update hero
// @Tags Hero
// @Accept json
// @Produce json
// @Security Bearer
// @Param id path string true "Hero Id"
// @Param body body swagger.PayloadHero true "Payload"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /hero/{id} [put]
// Handler for update hero
func (c *heroController) Put(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "Hero/Put"

	id := chi.URLParam(r, "id")

	payload := models.HeroModel{}
	if err := helper.ParsePayload(r, &payload); err != nil {
		RenderError(rw, http.StatusUnprocessableEntity, err.Error())
		return
	}

	payload.Id, _ = primitive.ObjectIDFromHex(id)

	res, err := c.repo.Update(helper.MongoFilter(helper.FoEq, "_id", payload.Id), payload)
	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
	}

	RenderSuccess(rw, http.StatusOK, res)
	// helper.RestLog(r, res.StatusCode(), logLocation, fmt.Sprintf("create hero with id %s success", body.(map[string]interface{})["data"]))
}

// Post godoc
// @Summary Create hero
// @Description Create a new hero
// @Tags Hero
// @Accept json
// @Produce json
// @Security Bearer
// @Param body body swagger.PayloadHero true "Payload"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /hero [post]
// Handler for create new hero
func (c *heroController) Post(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "Hero/CreateAdmin"

	payload := models.HeroModel{}
	if err := helper.ParsePayload(r, &payload); err != nil {
		RenderError(rw, http.StatusUnprocessableEntity, err.Error())
		return
	}

	res, err := c.repo.Insert(&payload)

	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		// helper.RestLog(r, res.StatusCode(), logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, res)
	// helper.RestLog(r, res.StatusCode(), logLocation, fmt.Sprintf("create hero with id %s success", body.(map[string]interface{})["data"]))
}

// Delete godoc
// @Summary Delete hero
// @Description Delete hero
// @Tags Hero
// @Accept json
// @Produce json
// @Security Bearer
// @Param id path string true "Hero Id"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /hero/{id} [delete]
// Handler for delete hero
func (c *heroController) Delete(rw http.ResponseWriter, r *http.Request) {
	// logLocation := "Hero/Delete"

	id := chi.URLParam(r, "id")

	res, err := c.repo.Delete(id)

	if err != nil {
		RenderError(rw, helper.MessageCode(err.Error()), err.Error())
		// helper.RestLog(r, res.StatusCode(), logLocation, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, res)
	// helper.RestLog(r, res.StatusCode(), logLocation, fmt.Sprintf("create hero with id %s success", body.(map[string]interface{})["data"]))
}
