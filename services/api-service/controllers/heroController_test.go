package controllers_test

import (
	"go-mongo/global/helper"
	"go-mongo/services/api-service/controllers"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-chi/chi/v5"
	"github.com/stretchr/testify/assert"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

func getHeroController(r *chi.Mux) {
	repo := NewHeroRepositoryMock(&mongo.Database{})
	controllers.NewHeroController(repo).Register(r)
}

func TestGets(t *testing.T) {
	testCase := []struct {
		Name         string
		Params       string
		Body         io.Reader
		ExpectedBody string
	}{
		{
			Name:         "Success",
			Params:       "?skip=0&limit=10",
			Body:         nil,
			ExpectedBody: "Ironman",
		},
		{
			Name:         "Failed",
			Params:       "?skip=0&limit=0",
			Body:         nil,
			ExpectedBody: helper.NewMessage().DataNotFound,
		},
		{
			Name:         "With Filter",
			Params:       "?skip=0&limit=10&name=ironman&group=marvel",
			Body:         nil,
			ExpectedBody: "Ironman",
		},
	}

	prepareTest()

	r := chi.NewRouter()
	getHeroController(r)
	ts := httptest.NewServer(r)
	defer ts.Close()

	for _, tc := range testCase {
		t.Run(tc.Name, func(t *testing.T) {
			res, body := testRequest(t, ts, http.MethodGet, "/hero"+tc.Params, bson.M{
				"Authorization": jwtTest,
			}, nil)

			if res.StatusCode == http.StatusOK {
				assert.Equal(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			} else {
				assert.NotEqual(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			}
		})
	}
}

func TestGet(t *testing.T) {
	testCase := []struct {
		Name         string
		Params       string
		Body         io.Reader
		ExpectedBody string
	}{
		{
			Name:         "Success",
			Params:       "/" + heroes[0].Id.Hex(),
			Body:         nil,
			ExpectedBody: "Ironman",
		},
		{
			Name:         "Failed: Not Found",
			Params:       "/" + helper.NewMessage().DataNotFound,
			Body:         nil,
			ExpectedBody: helper.NewMessage().DataNotFound,
		},
	}

	prepareTest()

	r := chi.NewRouter()
	getHeroController(r)
	ts := httptest.NewServer(r)
	defer ts.Close()

	for _, tc := range testCase {
		t.Run(tc.Name, func(t *testing.T) {
			res, body := testRequest(t, ts, http.MethodGet, "/hero"+tc.Params, bson.M{
				"Authorization": jwtTest,
			}, nil)

			if res.StatusCode == http.StatusOK {
				assert.Equal(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			} else {
				assert.NotEqual(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			}
		})
	}
}

func TestPut(t *testing.T) {
	testCase := []struct {
		Name         string
		Params       string
		Body         interface{}
		ExpectedBody string
		ExpectedCode int
	}{
		{
			Name:         "Success",
			Params:       "/" + heroes[0].Id.Hex(),
			Body:         heroes[0],
			ExpectedBody: "true",
			ExpectedCode: http.StatusOK,
		},
		{
			Name:         "Invalid Payload",
			Params:       "/" + heroes[0].Id.Hex(),
			Body:         "Invalid Payload",
			ExpectedBody: helper.NewMessage().UnmarshalFailed,
			ExpectedCode: http.StatusUnprocessableEntity,
		},
		{
			Name:   "Failed: Not Found",
			Params: "/" + heroes[0].Id.Hex(),
			Body: bson.M{
				"Name": helper.NewMessage().DataNotFound,
			},
			ExpectedBody: helper.NewMessage().DataNotFound,
			ExpectedCode: http.StatusNotFound,
		},
	}

	prepareTest()

	r := chi.NewRouter()
	getHeroController(r)
	ts := httptest.NewServer(r)
	defer ts.Close()

	for _, tc := range testCase {
		t.Run(tc.Name, func(t *testing.T) {
			res, body := testRequest(t, ts, http.MethodPut, "/hero"+tc.Params, bson.M{
				"Authorization": jwtTest,
			}, tc.Body)

			if res.StatusCode == http.StatusOK {
				assert.Equal(t, res.StatusCode, tc.ExpectedCode)
				assert.Contains(t, body, tc.ExpectedBody)
			} else {
				assert.NotEqual(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			}
		})
	}
}

func TestPost(t *testing.T) {
	testCase := []struct {
		Name         string
		Params       string
		Body         interface{}
		ExpectedBody string
		ExpectedCode int
	}{
		{
			Name:         "Success",
			Body:         heroes[0],
			ExpectedBody: "true",
			ExpectedCode: http.StatusOK,
		},
		{
			Name:         "Invalid Payload",
			Body:         "Invalid Payload",
			ExpectedBody: helper.NewMessage().UnmarshalFailed,
			ExpectedCode: http.StatusUnprocessableEntity,
		},
		{
			Name: "Failed: Duplicate",
			Body: bson.M{
				"Name": helper.NewMessage().Duplicate,
			},
			ExpectedBody: helper.NewMessage().Duplicate,
			ExpectedCode: http.StatusInternalServerError,
		},
	}

	prepareTest()

	r := chi.NewRouter()
	getHeroController(r)
	ts := httptest.NewServer(r)
	defer ts.Close()

	for _, tc := range testCase {
		t.Run(tc.Name, func(t *testing.T) {
			res, body := testRequest(t, ts, http.MethodPost, "/hero", bson.M{
				"Authorization": jwtTest,
			}, tc.Body)

			if res.StatusCode == http.StatusOK {
				assert.Equal(t, res.StatusCode, tc.ExpectedCode)
				assert.Contains(t, body, tc.ExpectedBody)
			} else {
				assert.NotEqual(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			}
		})
	}
}

func TestDelete(t *testing.T) {
	testCase := []struct {
		Name         string
		Params       string
		Body         interface{}
		ExpectedBody string
		ExpectedCode int
	}{
		{
			Name:         "Success",
			Params:       "/" + heroes[0].Id.Hex(),
			ExpectedBody: "true",
			ExpectedCode: http.StatusOK,
		},
		{
			Name:         "Failed: Not Found",
			Params:       "/" + helper.NewMessage().DataNotFound,
			ExpectedBody: helper.NewMessage().DataNotFound,
			ExpectedCode: http.StatusNotFound,
		},
	}

	prepareTest()

	r := chi.NewRouter()
	getHeroController(r)
	ts := httptest.NewServer(r)
	defer ts.Close()

	for _, tc := range testCase {
		t.Run(tc.Name, func(t *testing.T) {
			res, body := testRequest(t, ts, http.MethodDelete, "/hero"+tc.Params, bson.M{
				"Authorization": jwtTest,
			}, tc.Body)

			if res.StatusCode == http.StatusOK {
				assert.Equal(t, res.StatusCode, tc.ExpectedCode)
				assert.Contains(t, body, tc.ExpectedBody)
			} else {
				assert.NotEqual(t, http.StatusOK, res.StatusCode)
				assert.Contains(t, body, tc.ExpectedBody)
			}
		})
	}
}
