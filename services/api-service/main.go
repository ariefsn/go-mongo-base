package main

import (
	"fmt"
	"go-mongo/global/helper"
	_ "go-mongo/global/models/swagger"
	"go-mongo/services/api-service/controllers"
	"go-mongo/services/api-service/repositories"
	"log"
	"net/http"
	"strings"

	. "go-mongo/services/api-service/docs"

	logger "github.com/chi-middleware/logrus-logger"
	"github.com/common-nighthawk/go-figure"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/sirupsen/logrus"
	httpSwagger "github.com/swaggo/http-swagger"
)

// @version 3.0

// @contact.name API Support
// @contact.url https://www.ariefsn.dev
// @contact.email ayiexz22@gmail.com

// @BasePath /
// @Security BearerAuth
// @securityDefinitions.apiKey  Bearer
// @in header
// @name Authorization

func main() {
	err := helper.InitEnv()

	if err != nil {
		log.Fatal("Error loading .env file")
	}

	err = helper.DBPing()

	if err != nil {
		log.Fatal("Ping database failed. Error:", err)
	}

	db, err := helper.DBInstance()

	if err != nil {
		log.Fatal("Create database instance failed. Error:", err)
	}

	helper.InitRender()
	helper.InitJwt()

	env := helper.GetEnv()

	SwaggerInfo.Title = env.ServiceName
	SwaggerInfo.Description = fmt.Sprintf("%s API Documentation.", env.ServiceName)
	SwaggerInfo.BasePath = env.Swagger.BasePath

	r := chi.NewRouter()

	r.Use(middleware.Heartbeat("/ping"))
	r.Use(logger.Logger("router", logrus.New()))
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(helper.SecureSetup().Handler)

	// static
	// r.Handle("/download/", http.StripPrefix("/uploads", fs))

	// Repositories
	userRepo := repositories.NewUserRepository(db)
	heroRepo := repositories.NewHeroRepository(db)

	// Init Superuser Password
	userRepo.InitPasswordSuperUser()

	// Register Routes
	controllers.NewRootController().Register(r)
	controllers.NewUserController(userRepo).Register(r)
	controllers.NewHeroController(heroRepo).Register(r)

	// Register Swagger
	r.Group(func(r chi.Router) {
		r.Use(helper.CorsSetup(true).Handler)

		docsUrl := fmt.Sprintf("%s%s:%s/docs/doc.json", env.Swagger.Protocol, env.Swagger.Host, env.Swagger.Port)

		if strings.ToLower(env.Swagger.Port) == "none" {
			docsUrl = fmt.Sprintf("%s%s/docs/doc.json", env.Swagger.Protocol, env.Swagger.Host)
		}

		r.Get("/docs/*", httpSwagger.Handler(
			httpSwagger.URL(docsUrl),
		))
	})

	address := fmt.Sprintf("%s:%s", env.Host, env.Port)

	addressLog := fmt.Sprintf("Start on %s\n", address)

	figure.NewColorFigure(env.ServiceName, "", "green", true).Print()

	fmt.Println("\n", addressLog)

	helper.RouterConfig(r)

	err = http.ListenAndServe(address, r)

	if err != nil {
		log.Fatal("Serve failed. Error", err)
		return
	}
}
