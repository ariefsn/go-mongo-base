package models

import (
	"go-mongo/global/helper"
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type HeroType string

const (
	Good HeroType = "good"
	Bad           = "bad"
)

type HeroGroup string

const (
	Marvel HeroGroup = "marvel"
	Dc               = "dc"
)

type HeroModel struct {
	Id        primitive.ObjectID `json:"id" bson:"_id"`
	RealName  string             `json:"realName" bson:"realName"`
	Name      string             `json:"name" bson:"name"`
	Type      HeroType           `json:"type" bson:"type"`
	Group     HeroGroup          `json:"group" bson:"group"`
	CreatedAt time.Time          `json:"createdAt" bson:"createdAt"`
	UpdatedAt time.Time          `json:"updatedAt" bson:"updatedAt"`
}

func NewHeroModel() *HeroModel {
	m := new(HeroModel)
	m.CreatedAt = helper.GetTimeLocale().UTC()
	m.UpdatedAt = m.CreatedAt

	return m
}

func (m *HeroModel) TableName() string {
	return "heroes"
}

func (m *HeroModel) UpdateTime() *HeroModel {
	m.UpdatedAt = helper.GetTimeLocale().UTC()

	return m
}

func (m *HeroModel) Update(data *HeroModel) *HeroModel {
	m.Name = data.Name
	m.RealName = data.RealName
	m.Type = data.Type
	m.Group = data.Group
	m.UpdatedAt = helper.GetTimeLocale().UTC()

	return m
}
