# API SERVICE

> Service that will expose several endpoints from several services. The other services should not be exposed on production mode.

## How To

1. Copy `.env.example` to `.env`
2. Setup configuration `.env`
3. `go run main.go`
