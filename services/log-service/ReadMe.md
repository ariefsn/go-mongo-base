# LOG SERVICE

> Log Service.

## Features

- Write log to json file daily

## Log Model

```json
  {
    "id": "ec407e8c-78d2-43d6-9840-13994e54d4f1",
    "serviceName": "User Service",
    "extras": {},
    "location": "User/Login",
    "description": "login success",
    "status": 200,
    "createdAt": "2021-09-18T08:54:15.083684296+07:00"
  }
```

## How To

1. Copy `.env.example` to `.env`
2. Setup configuration `.env`
3. `go run main.go`
