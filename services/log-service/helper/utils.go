package helper

import (
	"encoding/json"
	"fmt"
	gmod "go-mongo/global/models"
	"io/ioutil"
	"path"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/google/uuid"
	"github.com/tidwall/gjson"
)

func GetPath() (dirPath, fileName string) {
	_, fileName, _, ok := runtime.Caller(0)

	if !ok {
		panic("No caller information")
	}

	dirPath = path.Dir(fileName)

	return
}

func GetAssetPath(fileName string) string {
	if !strings.Contains(fileName, ".json") {
		fileName += ".json"
	}

	return filepath.Join("logs", fileName)
}

func Parse(obj gjson.Result) gmod.LogModel {
	model := gmod.LogModel{}

	_ = json.Unmarshal([]byte(obj.Raw), &model)

	return model
}

func BuildQuery(filter gmod.LogModel) string {
	queries := []string{}

	if filter.Id != "" {
		queries = append(queries, fmt.Sprintf(`#(id%s"*%s*")#`, "%", filter.Id))
	}

	// if filter.IdCompany != "" {
	// 	queries = append(queries, fmt.Sprintf(`#(idCompany%s"*%s*")#`, "%", filter.IdCompany))
	// }

	// if filter.IdUser != "" {
	// 	queries = append(queries, fmt.Sprintf(`#(idUser%s"*%s*")#`, "%", filter.IdUser))
	// }

	if filter.Location != "" {
		queries = append(queries, fmt.Sprintf(`#(location%s"*%s*")#`, "%", filter.Location))
	}

	if filter.ServiceName != "" {
		queries = append(queries, fmt.Sprintf(`#(serviceName%s"*%s*")#`, "%", filter.ServiceName))
	}

	query := "*"

	if len(queries) > 0 {
		query = "data." + strings.Join(queries, "|")
	}

	return query
}

func Unique(logs []gmod.LogModel) []gmod.LogModel {
	keys := make(map[string]bool)
	list := []gmod.LogModel{}

	for _, a := range logs {
		if _, value := keys[a.Id]; !value {
			keys[a.Id] = true
			list = append(list, a)
		}
	}

	return list
}

func GetLogData(fileName string, filters []gmod.LogModel, appendLogs ...gmod.LogModel) ([]gmod.LogModel, int) {
	file, _ := ioutil.ReadFile(GetAssetPath(fileName))

	if len(filters) == 0 {
		filters = []gmod.LogModel{
			{},
		}
	}

	all := gjson.GetBytes(file, "data.#")

	res := []gjson.Result{}

	for _, v := range filters {
		res = append(res, gjson.Get(string(file), BuildQuery(v)).Array()...)
	}

	datas := []gmod.LogModel{}

	for _, v := range res {
		datas = append(datas, Parse(v))
	}

	if len(appendLogs) > 0 {
		for _, newLog := range appendLogs {
			if newLog.Id == "" {
				newLog.Id = uuid.NewString()
			}
			datas = append(datas, newLog)
		}
	}

	return Unique(datas), int(all.Int())
}

func SaveLog(fileName string, data []gmod.LogModel) error {
	buildData := struct {
		Data []gmod.LogModel `json:"data"`
	}{
		Data: data,
	}

	file, _ := json.MarshalIndent(buildData, "", "\t")

	if !strings.Contains(fileName, ".json") {
		fileName += ".json"
	}

	// check log file
	filePath := filepath.Join("logs", fileName)

	return ioutil.WriteFile(filePath, file, 0644)
}
