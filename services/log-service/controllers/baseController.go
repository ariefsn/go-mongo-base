package controllers

import (
	"go-mongo/global/helper"
	gmod "go-mongo/global/models"
	"io"
)

type BaseController struct{}

var Msg = helper.NewMessage()

func RenderForward(w io.Writer, code int, data interface{}) {
	helper.Render().JSON(w, code, data)
}

func RenderSuccess(w io.Writer, code int, data interface{}) {
	m := gmod.NewResponseModel()

	m.Success = true
	m.Data = data

	helper.Render().JSON(w, code, m)
}

func RenderSuccessList(w io.Writer, code int, data interface{}, total int) {
	m := gmod.NewResponseModel()

	m.Success = true
	m.Data = map[string]interface{}{
		"items": data,
		"total": total,
	}

	helper.Render().JSON(w, code, m)
}

func RenderError(w io.Writer, code int, message string) {
	m := gmod.NewResponseModel()

	m.Success = false
	m.Message = message

	helper.Render().JSON(w, code, m)
}
