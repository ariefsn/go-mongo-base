package controllers

import (
	"go-mongo/global/helper"
	gmod "go-mongo/global/models"
	rep "go-mongo/services/log-service/repositories"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
)

var logRep = rep.NewLogRepository()

type LogController struct {
	BaseController
}

func NewLogController() *LogController {
	return new(LogController)
}

func (c *LogController) Register(r *chi.Mux) *chi.Mux {
	r.Group(func(r chi.Router) {
		r.Post("/create", c.Create)
	})

	return r
}

// Create godoc
// @Summary Create log
// @Description Create a new company and new owner
// @Tags Log
// @Accept json
// @Produce json
// @Param body body swagger.PayloadLog true "Payload"
// @Success 200 {object} swagger.ResponseSuccessText
// @Failure 400,404,500 {object} swagger.ResponseError
// @Router /create [post]
// Handler for create new company and owner
func (c *LogController) Create(rw http.ResponseWriter, r *http.Request) {
	payload := gmod.LogModel{}

	if err := helper.ParsePayload(r, &payload); err != nil {
		RenderError(rw, http.StatusUnprocessableEntity, err.Error())
		return
	}

	payload.CreatedAt = time.Now().In(helper.GetTimezone())

	idLog, err := logRep.Insert(payload)

	if err != nil {
		RenderError(rw, http.StatusInternalServerError, err.Error())
		return
	}

	RenderSuccess(rw, http.StatusOK, idLog)
}
