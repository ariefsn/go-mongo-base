package repositories

import (
	"go-mongo/global/helper"
	gmod "go-mongo/global/models"
	locHelper "go-mongo/services/log-service/helper"
	"os"
	"path/filepath"
	"time"

	"github.com/google/uuid"
)

type LogRepository interface {
	Insert(data gmod.LogModel) (string, error)
}

type logRepository struct{}

func NewLogRepository() LogRepository {
	return new(logRepository)
}

func (c *logRepository) Insert(data gmod.LogModel) (string, error) {
	fileName := time.Now().In(helper.GetTimezone()).Format("2006-01-02") + ".json"

	// check log file
	filePath := filepath.Join("logs", fileName)

	if _, err := os.Stat(filePath); os.IsNotExist(err) {
		locHelper.SaveLog(fileName, []gmod.LogModel{})
	}

	data.Id = uuid.NewString()

	datas, _ := locHelper.GetLogData(fileName, nil, data)

	err := locHelper.SaveLog(fileName, datas)

	if err != nil {
		return "", err
	}

	return data.Id, nil
}
